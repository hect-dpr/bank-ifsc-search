# Django REST Framework Bank IFSC Search with JWT
(Built atop of [this](https://github.com/sibtc/drf-jwt-example))
[![Python Version](https://img.shields.io/badge/python-3.7-brightgreen.svg)](https://python.org)
[![Django Version](https://img.shields.io/badge/django-2.1-brightgreen.svg)](https://djangoproject.com)
[![Django Rest Framework Version](https://img.shields.io/badge/djangorestframework-3.9-brightgreen.svg)](https://www.django-rest-framework.org/)

Using PostgreSQL as a backend database with this [data](https://github.com/snarayanank2/indian_banks/blob/master/indian_banks.sql).

GET API to fetch a bank details, given branch IFSC code

GET API to fetch all details of branches, given bank name and a city with support limit & offset parameters

APIs should be authenticated using a JWT key, with validity = 5 days.

Example test run:

```bash
curl -i -X GET \
   -H "Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTcwNzk0MzA1LCJqdGkiOiI5NTEwOWQzZTY4NjQ0MzdlYTk3ZjNiNjljZGViM2ZmOSIsInVzZXJfaWQiOjF9.TBMk8wdM99_nsMg4mdw-Tp_3NUzVMviUXVZejKunwl4" \
 'https://boiling-earth-25954.herokuapp.com/hello/?bank_name=UNITED+BANK+OF+INDIA&city=BHUBANESWAR&offset=2&limit=5'
 
curl -i -X GET \
   -H "Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTcwNzk0MzA1LCJqdGkiOiI5NTEwOWQzZTY4NjQ0MzdlYTk3ZjNiNjljZGViM2ZmOSIsInVzZXJfaWQiOjF9.TBMk8wdM99_nsMg4mdw-Tp_3NUzVMviUXVZejKunwl4" \
 'https://boiling-earth-25954.herokuapp.com/hello/?ifsc=VIJB0002029'
```

For new tokens visit:

https://boiling-earth-25954.herokuapp.com/api/token/

Username: admin

Password: pass

## Running the Project Locally

First, clone the repository to your local machine:

```bash
git clone https://gitlab.com/hect-dpr/bank-ifsc-search.git
```

Install the requirements

```bash
pip install -r requirements.txt
```

Apply the migrations:

```bash
python manage.py migrate
```

Finally, run the development server:

```bash
python manage.py runserver
```

The API endpoints will be available at **127.0.0.1:8000/hello/**.


## License

The source code is released under the [MIT License].
