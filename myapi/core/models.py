from django.db import models

# Create your models here.
class Bank(models.Model):
    name = models.CharField(max_length=50)
    
    class Meta:
        db_table = 'banks'

class Branch(models.Model):
    ifsc = models.CharField(primary_key=True, max_length=12)
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    branch = models.CharField(max_length=75)
    address = models.CharField(max_length=195)
    city = models.CharField(max_length=50)
    district = models.CharField(max_length=50)
    state = models.CharField(max_length=26)
    
    class Meta:
        db_table = 'branches'
