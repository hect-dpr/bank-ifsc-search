from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.forms.models import model_to_dict

from myapi.core.models import Bank, Branch

class HelloView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        content = {'GET': request.GET}
        if request.GET.get('ifsc'):
            branch = Branch.objects.get(ifsc=request.GET.get('ifsc'))
            content = {
                        'bank_id':  branch.bank_id,
                        'bank_name':    branch.bank.name 
                    }
        elif request.GET.get('bank_name') and request.GET.get('city'):
            offset = int(request.GET.get('offset')) if request.GET.get('offset') else 0
            limit = int(request.GET.get('limit')) + offset if request.GET.get('limit') else 10000
            branches = Branch.objects.filter(bank__name=request.GET.get('bank_name'), city=request.GET.get('city')).values()[offset:limit]
            content['branches'] = branches
        else:
            content = {'message': 'Hello, World!'}
        return Response(content)
